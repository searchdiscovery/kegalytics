(function ($) {
  $.fn.toggleDisabled = function (disabled) {
    return this.each(function () {
      var $this = $(this);
      if (!disabled) {
        $this.removeAttr('disabled');
      } else {
        $this.attr('disabled', 'disabled');
      }
    });
  };

  $.fn.resetInputs = function () {
    return this.each(function () {
      var $this = $(this);
      $this.find('input[type=radio]').removeAttr('checked');
      $this.find('input[type=text], input[type=number], select').val('');
    });
  };
})(jQuery);

$('.kegIsActive').click(function () {
  var $fieldset = $('fieldset#' + $(this).attr('data-forfieldset'));
  $fieldset.toggleDisabled(!$(this).is(':checked'));
  $fieldset.resetInputs();
});

$('.reset').click(function (e) {
  e.preventDefault();
  var $fieldset = $('fieldset#' + $(this).attr('data-forfieldset'));
  $fieldset.removeAttr('disabled');
  $fieldset.resetInputs();
});

$('form').submit(function (e) {
  e.preventDefault();

  var $this = $(this),
      data = {};
  $this.serializeArray().forEach(function (elem) {
    data[elem.name] = elem.value;
  });

  $.post($this.attr('action'), data, function () {
    console.log('Keg info updated');
  });
});

$('.kegVolumeSelect').change(function () {
  var $fieldset = $('fieldset#' + $(this).attr('data-forfieldset'));
  var volume = $(this).val();
  $fieldset.find('.remaining').val(volume).attr('max', volume);
});
