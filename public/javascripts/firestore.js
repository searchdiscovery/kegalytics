const Firestore = require('@google-cloud/firestore');
const { initializeApp } = require('firebase-admin/app');
const { Timestamp, FieldValue, getFirestore } = require('firebase-admin/firestore');

let firestore = new Firestore({ projectId: 'helpful-helper-516'})


// initializeApp({ projectId: 'helpful-helper-516'})
const db = getFirestore()


const vals = async function() {
    const kegRef = db.collection('kegalytics');
    snapshot = await kegRef.get()
    let fin = []    
    snapshot.forEach(x => {
        fin.push(x.data())
    })
    return fin

}
const reset = async function(data, keg) {
    const document = (keg == "left_keg") ? "Keg_1" : "Keg_2"

    data['Name'] = keg
    data['Location'] = 'Atlanta'
    data['size'] = data['volume'] 
    data['maxCapacity'] = 100
    data['currentCapacity'] = 100
    data['event'] = 'reset'
    const doc_ref = db.collection('kegalytics').doc(document)
    const resp = await doc_ref.set(
        data
    )
    return data
}

module.exports = {
    reset,
    vals

}