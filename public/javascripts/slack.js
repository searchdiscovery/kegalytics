const fetch = require('node-fetch');
// import fetch from 'node-fetch';

// var myHeaders = new Headers();
// myHeaders.append("Content-Type", "application/json");



const slackMessage = async function(data) {
    let vowels = ['a','e','i','o','u']
    let letter = data.style.toString().toLowerCase()[0]
    let article = (vowels.indexOf(letter) != -1)? 'an': 'a';
    let text = `A new keg from ${data.breweryName} in ${data.breweryLocation} was added! \n ${data.name}, is ${article} ${data.style} and is ${data.abv}% abv`
    let raw = JSON.stringify({
        text
      });
      let requestOptions = {
        method: 'POST',
      //   headers: myHeaders,
        body: raw,
        redirect: 'follow'
      };
   
   
   
    const resp = await fetch("https://hooks.slack.com/services/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", requestOptions)
   return resp.text();
} 


// fetch("https://hooks.slack.com/services/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", requestOptions)
//   .then(response => response.text())
//   .then(result => console.log(result))
//   .catch(error => console.log('error', error));

module.exports = {slackMessage}; 