$(document).foundation({
  reveal: {
    animation: 'none'
  }
});

var $kegs;

function getKegs () {
  $kegs = [{
    _root: $('#keg0')
  }, {
    _root: $('#keg1')
  }];

  $kegs.forEach(function ($keg, i) {
    $kegs[i].beer = $keg._root.find('.beer');
    $kegs[i].percent = $keg._root.find('.percent');
    $kegs[i].pours = $keg._root.find('.pours');
  });
}
getKegs();

var $pouringName = $('#pouring .name');
var $pouringOz = $('#pouring .oz');

var socket = io();
socket.on('keg update', function (keg) {
  var $keg = $kegs[keg.id]._root;
  $.get('/kegs/' + keg.id, keg, function (layout) {
    $keg.html(layout);
    getKegs();
  });
});

function updateKegContents(keg) {
  var $keg = $kegs[keg.id];
  var newPos = -(82 - ((keg.remaining / keg.volume) * 82)) + '%';

  $keg.beer.css('bottom', newPos);
  $keg.percent.html((Math.floor(keg.remaining / keg.volume * 1000) / 10) + '<sup>%</sup>');
}

function updatePours (keg) {
  var $keg = $kegs[keg.id];
  $keg.pours.html(keg.pours);
}

function updateTemperature (temp) {
  $('#temp').text(temp);
}

socket.on('temp', function (data) {
  updateTemperature(data.temp);
});

socket.on('pour:start', function (data) {
  $pouringName.text(data.keg.name);
  showModal();
});

socket.on('pour:progress', function (data) {
  $pouringOz.text(data.cumulativeFlow);
  updateKegContents(data.keg);
});

socket.on('pour:end', function (data) {
  hideModal();
  updatePours(data.keg);
});

function formattedTime(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  var strTime = hours + ':' + minutes + ampm;
  return strTime;
}

function showModal () {
  $('#pouring').foundation('reveal', 'open');
}

function hideModal () {
  $('#pouring').foundation('reveal', 'close');
}

function printTime () {
  var time = formattedTime(new Date());
  $('#time').text(time);
}

printTime();

setInterval(printTime, 15000);
