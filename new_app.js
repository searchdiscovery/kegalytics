require('dotenv').config();
const app = module.exports = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;
const fs = require('fs');
const { vals, listener } = require("./test");
const path = require('path');
const {router, static} = require('express')
const helper = require('./worker.js')
const {reset} = require('./public/javascripts/firestore')
const { slackMessage } = require('./public/javascripts/slack')

const {json} = require('express')
const {urlencoded} = require('express')

const Firestore = require('@google-cloud/firestore');
const { Timestamp, FieldValue, getFirestore } = require('firebase-admin/firestore');

let firestore = new Firestore({ projectId: 'helpful-helper-516'})
// initializeApp({ projectId: 'helpful-helper-516'})
const db = getFirestore()

io.on('connection', (socket) => {
    socket.on('chat message', msg => {
      io.emit('chat message', msg);
    });
  });


app.set('view engine', 'pug');
app.use('/img', static(__dirname + '/public/img'));
app.use('/stylesheets', static(__dirname + '/public/stylesheets'));

app.use(json());       // to support JSON-encoded bodies
app.use(urlencoded({ extended: true })); // to support URL-encoded bodies

app.use(function (req, res, next) {
    helper  // to load in the helper functions to the web page
    next()
  });



const startServer = async function() {
    // start listener
    let kegs = await  vals()

    app.get('/worker.js',function(req,res){
        res.sendFile(path.join(__dirname + '/worker.js')); 
    });

    app.get('/atlanta', function(req, res) {
        vals()
            .then(x => res.render(path.join(__dirname +'/views/index.pug'), {kegs:x} ));
      });
    
    app.get('/cleveland', function(req, res) {
        res.render(path.join(__dirname +'/views/index.pug'), {kegs:kegs} );
      });
    
    app.get('/setup/atlanta', function(req,res){
        vals()
            .then(kegs => res.render(path.join(__dirname +'/views/admin.pug'), {kegs:kegs, helper}))
            .catch(err => console.log(err))
    })
    app.get('/leaderboard', function(req, res){
        vals()
            .then(x => res.render(path.join(__dirname +'/views/leaderboard.pug'), {stats:x[2], helper}))
            .catch(err => console.log(err))
        // const tmp = await db.collection('kegalytics').where('Location', '==', 'Atlanta').get('pours');
        
    })
    
    app.get('/test', function(req, res){
        res.render(path.join(__dirname +'/views/test.pug'))
    })

    app.post('/setup/kegs/0', function(req, res){
        if(req.body) {
            reset(req.body, 'right_keg')
                .then(x => slackMessage(x))
                .catch(err => console.log(err))
        }
        res.redirect('/setup/atlanta')
        // res.render(path.join(__dirname +'/views/admin.pug'), {kegs:kegs, helper})
    })
    app.post('/setup/kegs/1', function(req, res){
        if(req.body) {
            reset(req.body, 'left_keg')
                .then(x => slackMessage(x))
                .catch(err => console.log(err))
        }        
        res.redirect('/setup/atlanta')
        // res.render(path.join(__dirname +'/views/admin.pug'), {kegs:kegs, helper})
    })


    io.on('connection', (socket) => {
        console.log('a user connected')

        const query = db.collection('kegalytics').where('Location', '==', 'Atlanta');
        const observer = query.onSnapshot(querySnapshot => {
        console.log(`Received query snapshot of size ${querySnapshot.size}`);

        querySnapshot.forEach(x => {
            socket.emit('pour', x.data())
        })
            // ...
          }, err => {
            console.log(`Encountered error: ${err}`);
          });    

        socket.on('pour', msg => {
            io.emit('pour', msg);
        });
    });

    // app.use('/', router);  
    http.listen(port, () => console.log(`Example app listening on port ${port}!`));
}


startServer()
    .then(x => console.log(x))
    .catch(err => console.log(err))

