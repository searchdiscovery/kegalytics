var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var Store = require('./lib/store');
var track = require('./lib/track');
var tweet = require('./lib/tweet');

var routes = require('./routes/index');

var app = express();

var server = require('http').Server(app);
var io = require('socket.io')(server);
var kegs = null;
Store(__dirname + '/store.sqlite').then(k => {
  kegs = k
  if (process.env.OS === 'pi') {
    pinControls(kegs)
  }
})

app.use(function (req, res, next) {
  req.store = kegs;
  next();
});

app.use(function (req, res, next) {
  req.io = io;
  next();
});

const pinControls = (kegs) => {
  var ua = app.get('env') === 'production' ? 'UA-12008979-21' : 'UA-12008979-20';
  var TapMeter = require('./lib/tapmonitor');
  var therm = require('./lib/therm');

  setInterval(function () {
    therm.getTemp('F').then(function (temp) {
      io.emit('temp', { temp: temp });
    });
  }, 5000);

  var pins = {
    flow: [2, 14]
  };

  kegs.all().then(function (kegsData) {

    kegsData.forEach(function (k, i) {
      var meter = new TapMeter(pins.flow[i]);
      meter.on('pour:start', function () {
        console.log('pour:start');
        kegs.get(k.id).then(function (keg) {
          io.emit('pour:start', { keg: keg });
        });
      });

      meter.on('pour:progress', function (data) {
        kegs.get(k.id).then(function (keg) {
          keg.remaining -= data.flow;
          kegs.set(keg.id, keg);
          io.emit('pour:progress', {
            keg: keg,
            cumulativeFlow: data.cumulativeFlow
          });
        });
      });

      meter.on('pour:end', function (totalFlow) {
        kegs.get(k.id).then(function (keg) {
          keg.pours = keg.pours || 0;
          if (totalFlow > 0.1) {
            keg.pours += 1;
          }
          kegs.set(keg.id, keg);
          console.log('pour:end', totalFlow);
          io.emit('pour:end', {
            keg: keg,
            totalFlow: totalFlow
          });

          if (totalFlow > 1.5) {
            var data = {
              keg: keg,
              pour: {
                oz: totalFlow
              },
              temp: 35
            };

            track(ua, data).then(function () {
              console.log('tracked pour info');
            }, function (err) {
              console.log('tracking error', err);
            });

            var tweetStr = 'Just poured {{pour.oz}}oz of {{keg.breweryName}} {{keg.name}}.';

            tweet(tweetStr, data).then(function () {
              console.log('tweeted pour info');
            });
          }
        });
      });
    });
  });
}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

if (app.get('env') === 'development') {
  app.use(logger('dev'));
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/vendor', express.static(__dirname + '/bower_components'));

app.use('/', routes);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = {
  app: app,
  server: server
};
