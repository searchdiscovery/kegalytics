from HX711py import *
from helpers import *
import os
class kegalytics:
    def __init__(self, beer_name):
        self.active_range = []
        self.beer_name = ''
        self.pours = 0

# EOF - hx711.py
db = firestore.Client()
transaction = db.transaction()
name = 'left_keg'
beer_name = 'test'
threshold = 50
full_weight = 4870
catalog = [0, 0, 0, 0 ]
current_weight = 0
benchmark_weight = 0
mid_pour = False

def diff(x,y):
    z = x - y
    return z

hx = HX711(5, 6)
hx.set_reading_format("MSB", "MSB")
hx.set_reference_unit(10)

hx.reset()

hx.tare()

print("Tare done! Add weight now...")


while True:
    try:
        # These three lines are usefull to debug wether to use MSB or LSB in the reading formats
        # for the first parameter of "hx.set_reading_format("LSB", "MSB")".
        # Comment the two lines "val = hx.get_weight(5)" and "print val" and uncomment these three lines to see what it prints.
        
        # np_arr8_string = hx.get_np_arr8_string()
        # binary_string = hx.get_binary_string()
        # print binary_string + " " + np_arr8_string
        
        # Propriatary Weight Calculations
        val = hx.get_weight(5)
        val -= 40
        val /= 2.2
        diff_weight = diff(current_weight, val)
        full_percent = val/full_weight
        
	# Filters out very errant results (Doubling)
        tester = full_percent - catalog[3] ## test if a scale is throwing a wild value
        if  tester < 1:
            catalog.append(full_percent)
            curr_avg = sum(catalog[-4:]) / 4
        
            
            print("%: {:3}, C.Avg: {:3}, Diff: {}".format(round(full_percent, 3), round(curr_avg,3), round(full_percent - curr_avg, 3)))
            #
            if mid_pour == False and full_percent - curr_avg < .005 and round(full_percent - curr_avg, 2) != 0:
                mid_pour = True
                benchmark_weight = catalog[0]
                print('Pour Started at {}'.format(benchmark_weight))
            
            if mid_pour == True and round(full_percent - curr_avg, 3) == 0:
                mid_pour = False
                update_in_transaction(transaction, curr_avg)
                print('Pour Ended at {}'.format(curr_avg))
                
            #    print('Pour Ended At {}'.format(val)) 
            #    print('Pour Totaled {}'.format(diff(benchmark_weight,val)))
            #    benchmark_weight = val
            
            catalog.pop(0)
        else:
            print('skipped')
        #if mid_pour == False and diff_weight > threshold:
        #    mid_pour = True
        #    print('Pour Started at {}'.format(benchmark_weight))
        #    #benchmark_weight = val
        #if mid_pour == True and diff_weight < threshold:
        #    mid_pour = False
        #    print('Pour Ended At {}'.format(val)) 
        #    print('Pour Totaled {}'.format(diff(benchmark_weight,val)))
        #    benchmark_weight = val
        # To get weight from both channels (if you have load cells hooked up 
        # to both channel A and B), do something like this
        #val_A = hx.get_weight_A(5)
        #val_B = hx.get_weight_B(5)
        #print "A: %s  B: %s" % ( val_A, val_B )

        hx.power_down()
        hx.power_up()
        time.sleep(0.1)
        current_weight = val

    except (KeyboardInterrupt, SystemExit):
        cleanAndExit()

