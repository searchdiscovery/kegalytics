import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

cred = credentials.ApplicationDefault()
firebase_admin.initialize_app(cred, {
  'projectId': 'helpful-helper-516',
})

db = firestore.Client()

transaction = db.transaction()
doc_ref = db.collection('kegalytics').document('Keg_1')
board_ref = db.collection('kegalytics').document('leaderboard')

@firestore.transactional
def update_in_transaction(transaction, remaining):
    print('transacting', remaining)
    snapshot = doc_ref.get(transaction=transaction)
    transaction.update(doc_ref, {
        u'pours': int(snapshot.get(u'pours')) +1 ,
        u'currentCapacity': remaining * 100
})
    transaction.update(board_ref, {
        u'atlanta_pours': int(snapshot.get(u'atlanta_pours')) +1 
})
#update_in_transaction(transaction, doc_ref)
