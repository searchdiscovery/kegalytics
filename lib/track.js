var request = require('request');
var crypto = require('crypto');
var Q = require('q');

function randomHash () {
  return crypto.createHash('md5').update('kegalyticsWoot!' + (Math.random() * 10000)).digest('hex');
}

function parameterize (data) {
  return {
    tid:  data.ua,
    cid:  randomHash(),
    an:   'kegalytics',
    av:   1.0,
    t:    'event',
    v:    1,
    ev:   1,
    aid:  'kegalytics',
    ec:   data.keg.breweryName,
    ea:   data.keg.name,
    el:   data.pour.oz,
    cd1:  data.keg.breweryName,
    cd2:  data.keg.name,
    cd3:  data.keg.breweryLocation,
    cd4:  data.keg.abv,
    cd5:  data.keg.type,
    cd6:  data.keg.color,
    cd7:  data.keg.remaining,
    cd8:  data.temp,
    cd9:  data.keg.volume,
    cd10: data.keg.pours,
    cm1:  data.pour.oz,
    cm2:  data.keg.cost * (data.pour.oz / data.keg.volume),
    cm3:  1
  };
}

function track (ua, data) {
  var deferred = Q.defer();
  data.ua = ua;
  request({
    uri:    'http://google-analytics.com/collect',
    method: 'POST',
    form:   parameterize(data),
    headers: {
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36'
    }
  }, function (err, response, body) {
    if (err) {
      deferred.reject(err);
    }
    deferred.resolve(response);
  });
  return deferred.promise;
}

module.exports = track;
