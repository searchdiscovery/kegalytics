var env = process.env.NODE_ENV || 'development';
var TwitterApp = require('twitter');
var twitterConfig = require('../twitter.json');
var twitter = new TwitterApp(twitterConfig[env]);
var _ = require('lodash');
var Q = require('q');

_.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

function tweet (str, data) {
  var deferred = Q.defer();
  var tweetStr = _.template(str)(data);

  twitter.post('/statuses/update.json', { status: tweetStr }, function (data) {
    deferred.resolve(data);
  });

  return deferred.promise;
}

module.exports = tweet;
