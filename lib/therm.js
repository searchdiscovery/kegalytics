var exec = require('child_process').exec;
var sense = require('ds18b20');
var Q = require('q');

exec('modprobe wire');
exec('modprobe w1-gpio');
exec('modprobe w1-therm');

function convertCtoF (tempC, places) {
  places = places === undefined ? 1 : places;
  return roundToDecimals((tempC * 9 / 5) + 32, places);
}

function average (arr, places) {
  places = places === undefined ? 1 : places;
  var sum = 0;
  for (var i = 0; i < arr.length; i++) {
    sum += arr[i];
  }
  return roundToDecimals(sum / arr.length, places);
}

function roundToDecimals(num, places) {
  var x = Math.pow(10, places);
  return Math.round(num * x) / x;
}

function getSensorsPromise () {
  var deferred = Q.defer();

  sense.sensors(function (err, ids) {
    if (err) {
      deferred.reject(err);
    }
    deferred.resolve(ids);
  });

  return deferred.promise;
}

var getSensors = getSensorsPromise();

function getTempFromSensor (sensor, places) {
  places = places === undefined ? 1 : places;
  var deferred = Q.defer();
  sense.temperature(sensor, function (err, value) {
    if (err) {
      deferred.reject(err);
    }
    deferred.resolve(roundToDecimals(value, places));
  });
  return deferred.promise;
}

function getTemp (cOrF) {
  var temps = getSensors.then(function (sensors) {
    return Q.all(sensors.map(function (sensor) {
      return getTempFromSensor(sensor);
    }));
  });
  return temps.then(function (tempsArray) {
    var temp = average(tempsArray);
    return cOrF === 'F' ? convertCtoF(temp) : temp;
  });
}

module.exports = { getTemp: getTemp };
