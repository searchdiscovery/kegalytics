// flow meter
var util = require('util'),
    EventEmitter = require('events').EventEmitter,
    _ = require('lodash');

// Constants
var OZ_IN_LITER = 33.814,
    SECONDS_IN_MINUTE = 60,
    MS_IN_SECOND = 1000,
    MIN_HZ = 0.25,
    MAX_HZ = 80,
    DECIMALS_ALLOWED = 2,
    PULSE_RATE = 7.5;

function roundToDecimals(num, places) {
  var x = Math.pow(10, places);
  return Math.round(num * x) / x;
}

function round (num) {
  return roundToDecimals(num, DECIMALS_ALLOWED);
}

function FlowMeter () {
  this.clicks = 0;
  this.lastClick = new Date().getTime();
  this.clickDelta = 0;
  this.hertz = 0;
  this.flow = 0;
  this.pour = 0;
}

FlowMeter.prototype.update = function (currentTime) {
  this.clicks++;
  this.clickDelta = _.max([currentTime - this.lastClick, 1]);

  if (this.clickDelta < MS_IN_SECOND) {
    this.hertz = MS_IN_SECOND / this.clickDelta;
    if (this.hertz > MIN_HZ && this.hertz < MAX_HZ) {
      this.flow = this.hertz / (SECONDS_IN_MINUTE * PULSE_RATE);
      var instPour = this.flow * (this.clickDelta / MS_IN_SECOND);
      this.pour += instPour;
    }
  }
  this.lastClick = currentTime;
};

FlowMeter.prototype.getFlow = function () {
  return round(this.flow * (this.clickDelta / MS_IN_SECOND) * OZ_IN_LITER);
};

FlowMeter.prototype.getPour = function () {
  return round(this.pour * OZ_IN_LITER);
};

FlowMeter.prototype.clear = function () {
  this.pour = 0;
  this.flow = 0;
  this.clickDelta = 0;
};

module.exports = FlowMeter;
