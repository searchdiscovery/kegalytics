const pour = function(docoument, data) {
    console.log(data);
    console.log(`Registered a ${data['event']} event`)

    if(data.Name.toLowerCase() == 'left_keg' && data.event == 'pour'){
        percent_left = data.currentCapacity / data.maxCapacity
        percent_left = percent_left * 100
        percent_left = `${percent_left}%`
        document.querySelectorAll('.small-5.columns.stats strong')[0].innerText = data.pours
        document.querySelectorAll('.small-5.columns.stats strong')[1].innerText = percent_left

    } else if(data.Name.toLowerCase() == 'right_keg' && data.event == 'pour'){
        percent_left = data.currentCapacity / data.maxCapacity
        percent_left = percent_left * 100
        percent_left = `${percent_left}%`
        document.querySelectorAll('.small-5.columns.stats strong')[2].innerText = data.pours
        document.querySelectorAll('.small-5.columns.stats strong')[3].innerText = percent_left
        
    } else if(data.Name.toLowerCase() == 'left_keg' && data.event == 'reset'){
        // console.log(`starting reset for ${data['Name']}`)
        console.log(data);
        const y = `${data['name']} ${data['breweryName']}`

        document.querySelectorAll('h2')[0].childNodes[0].textContent = y
        document.querySelectorAll('li')[0].innerText = data['breweryLocation']
        document.querySelectorAll('li')[1].innerText = `${data['abv']} %abv`
        document.querySelectorAll('li')[2].innerText = data['style']

        
        percent_left = data.currentCapacity / data.maxCapacity
        percent_left = percent_left * 100
        percent_left = `${percent_left}%`
        document.querySelectorAll('.small-5.columns.stats strong')[0].innerText = data['pours'] 
        document.querySelectorAll('.small-5.columns.stats strong')[1].innerText = percent_left

    } else if(data.Name.toLowerCase() == 'right_keg' && data.event == 'reset'){
        console.log(data)
        // console.log(`starting reset for ${data['Name']}`)
        document.querySelectorAll('h2')[1].childNodes[0].textContent = `${data['name']} ${data['breweryName']}`
        document.querySelectorAll('li')[3].innerText = data['breweryLocation']
        document.querySelectorAll('li')[4].innerText = `${data['abv']} %abv`
        document.querySelectorAll('li')[5].innerText = data['style']

        percent_left = data.currentCapacity / data.maxCapacity
        percent_left = percent_left * 100
        percent_left = `${percent_left}%`
        document.querySelectorAll('.small-5.columns.stats strong')[2].innerText = data['pours'] 
        document.querySelectorAll('.small-5.columns.stats strong')[3].innerText = percent_left
    }
}
